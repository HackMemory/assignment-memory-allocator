#define _DEFAULT_SOURCE

#include <unistd.h>
#include "util.h"
#include "mem.h"
#include "mem_internals.h"

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}
void debug(const char* fmt, ... );

static void* test_heap_init() {
    debug("Heap initializing started\n");
    void *heap = heap_init(10240);
    if (heap == NULL) {
        debug("Cannot initialize heap");
    }
    debug("OK. Heap initialized\n\n");
    return heap;
}

void test1 (struct block_header *first_block) {
    debug("Test 1 started:\n");

    const size_t test_size = 512;
    void *data1 = _malloc(test_size);
    if (data1 == NULL) {
        debug("Test 1 failed: _malloc returned NULL\n");
    }

    debug_heap(stdout, first_block);

    if (first_block->is_free) {
        debug("Test 1 failed: allocated_block isn't free\n");
    }
    if(first_block->capacity.bytes != test_size){
        debug("Test 1 failed: allocated_block size isn't correct\n");
    }
    debug("OK. Test 1 passed\n\n");

    _free(data1);
}

void test2(struct block_header *first_block) {
    debug("Test 2 started:\n");

    void *data1 = _malloc(512), *data2 = _malloc(600);
    if (data1 == NULL || data2 == NULL) {
        debug("Test 2 failed: _malloc returned NULL\n");
    }
    _free(data1);
    debug_heap(stdout, first_block);
    struct block_header *data_block1 = block_get_header(data1), *data_block2 = block_get_header(data2);
    if (!data_block1->is_free) {
        debug("Test 2 failed: free block is taken\n");
    }
    if (data_block2->is_free) {
        debug("Test 2 failed: taken block is free\n");
    }

    debug("OK. Test 2 passed\n\n");
    _free(data1);
    _free(data2);
}

void test3(struct block_header *first_block) {
    debug("Test 3 started:\n");

    const size_t s1 = 512, s2 = 1024, s3 = 2048;
    void *data1 = _malloc(s1), *data2 = _malloc(s2), *data3 = _malloc(s3);
    if (data1 == NULL || data2 == NULL || data3 == NULL) {
        debug("Test 3 failed: _malloc returned NULL\n");
    }

    _free(data2);
    _free(data1);
    debug_heap(stdout, first_block);
    struct block_header *data_block1 = block_get_header(data1), *data_block3 = block_get_header(data3);
    if (!data_block1->is_free) {
        debug("Test 3 failed: free block is taken\n");
    }
    if (data_block3->is_free) {
        debug("Test 3 failed: taken block is free\n");
    }
    if (data_block1->capacity.bytes != s1 + s2 + offsetof(struct block_header, contents)) {
        debug("Test 3 failed: two free blocks didn't connect\n");
    }
    debug("OK. Test 3 passed\n\n");

    _free(data1);
    _free(data2);
    _free(data3);
}

void test4(struct block_header *first_block) {
    debug("Test 4 started:\n");

    void *data1 = _malloc(10240), *data2 = _malloc(10240 + 512), *data3 = _malloc(2048);
    if (data1 == NULL || data2 == NULL || data3 == NULL) {
        debug("Test 4 failed: _malloc returned NULL\n");
    }
    _free(data3);
    _free(data2);

    debug_heap(stdout, first_block);

    struct block_header *data_block1 = block_get_header(data1), *data_block2 = block_get_header(data2);

    if ((uint8_t *)data_block1->contents + data_block1->capacity.bytes != (uint8_t*) data_block2){
        debug("Test 4 failed: new region wasn't created after last\n");
    }
    debug("OK. Test 4 passed\n\n");

    _free(data1);
    _free(data2);
    _free(data3);
}

void test5(struct block_header *first_block) {
    debug("Test 5 started:\n");

    void *data1 = _malloc(10000);
    if (data1 == NULL) {
        debug("Test 5 failed: _malloc returned NULL\n");
    }

    struct block_header *addr = first_block;
    while (addr->next != NULL) addr = addr->next;
    void* test_addr = (uint8_t*) addr + size_from_capacity(addr->capacity).bytes;
    test_addr = (uint8_t*) (getpagesize() * ((size_t) test_addr / getpagesize() + (((size_t) test_addr % getpagesize()) > 0)));
    test_addr = map_pages(test_addr, 1024, MAP_FIXED_NOREPLACE);
    void *data2 = _malloc(100000);

    debug_heap(stdout, first_block);

    struct block_header *data2_block = block_get_header(data2);
    if (data2_block == addr) {
        debug("Test 5 failed: new block wasn't allocated at new place\n");
    }
    debug("OK. Test 5 passed\n\n");

    _free(data1);
    _free(data2);
}


void test_all(){
	struct block_header *first_block = (struct block_header*) test_heap_init();

    test1(first_block);
    test2(first_block);
    test3(first_block);
    test4(first_block);
    test5(first_block);

}

int main() {
	test_all();
    return 0;
}
